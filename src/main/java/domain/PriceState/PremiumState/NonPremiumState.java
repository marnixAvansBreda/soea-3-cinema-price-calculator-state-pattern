package domain.PriceState.PremiumState;

import domain.PriceState.PriceState;

public class NonPremiumState implements PremiumState {

    private PriceState priceState;

    public NonPremiumState(PriceState priceState){
        this.priceState = priceState;
    }

    @Override
    public double getPremiumFee() {
        return 0;
    }

    @Override
    public void choosePremium() {
        priceState.setPremiumState(priceState.getRegularPremiumState());
    }

    @Override
    public void choosePremiumAsStudent() {
        priceState.setPremiumState(priceState.getStudentPremiumState());
    }
}
