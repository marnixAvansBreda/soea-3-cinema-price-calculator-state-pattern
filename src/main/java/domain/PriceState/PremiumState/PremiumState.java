package domain.PriceState.PremiumState;

public interface PremiumState {
    double getPremiumFee();
    void choosePremium();
    void choosePremiumAsStudent();
}
