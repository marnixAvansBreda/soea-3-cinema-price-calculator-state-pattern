package domain.PriceState.PremiumState;

import domain.PriceState.PriceState;

public class StudentPremiumState implements PremiumState {

    private PriceState priceState;

    public StudentPremiumState(PriceState priceState){
        this.priceState = priceState;
    }

    @Override
    public double getPremiumFee() {
        return 2;
    }

    @Override
    public void choosePremium() {
        priceState.setPremiumState(priceState.getRegularPremiumState());
    }

    @Override
    public void choosePremiumAsStudent() {
        System.out.println("Already student premium");
    }
}
