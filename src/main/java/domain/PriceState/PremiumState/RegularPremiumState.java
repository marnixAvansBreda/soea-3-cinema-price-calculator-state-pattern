package domain.PriceState.PremiumState;

import domain.PriceState.PriceState;

public class RegularPremiumState implements PremiumState {

    private PriceState priceState;

    public RegularPremiumState(PriceState priceState){
        this.priceState = priceState;
    }

    @Override
    public double getPremiumFee() {
        return 3;
    }

    @Override
    public void choosePremium() {
        System.out.println("Already regular premium");
    }

    @Override
    public void choosePremiumAsStudent() {
        priceState.setPremiumState(priceState.getStudentPremiumState());
    }
}
