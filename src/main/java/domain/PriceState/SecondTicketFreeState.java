package domain.PriceState;

import domain.MovieTicket;
import domain.Order;

import java.util.ArrayList;

public class SecondTicketFreeState extends PriceState {

    private Order order;

    public SecondTicketFreeState(Order order) {
        this.order = order;
    }

    @Override
    public double calculatePrice() {
        ArrayList<MovieTicket> tickets = order.getTickets();

        double price = 0;

        for (int i = 0; i < tickets.size(); i++) {
            if (i % 2 == 0)
                continue;
            double ticketPrice = tickets.get(i).getPrice() + getPremiumState().getPremiumFee();
            price += ticketPrice;
        }

        return price;
    }

    @Override
    public void addTicket() {
        System.out.println("Second ticket free is better than ten percent off");
    }

    @Override
    public void showStudentCard() {
        System.out.println("Already second ticket free");
    }

    @Override
    public void chooseWeekday() {
        System.out.println("Already second ticket free");
    }
}
