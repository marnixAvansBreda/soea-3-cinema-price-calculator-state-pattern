package domain.PriceState;

import domain.MovieTicket;
import domain.Order;
import domain.PriceState.PremiumState.PremiumState;

import java.util.ArrayList;

public class RegularPriceState extends PriceState {

    private Order order;
    private PremiumState premiumState;

    public RegularPriceState(Order order) {
        this.order = order;
    }

    @Override
    public double calculatePrice() {
        ArrayList<MovieTicket> tickets = order.getTickets();

        double price = 0;

        for(MovieTicket ticket : tickets) {
            double ticketPrice = ticket.getPrice() + getPremiumState().getPremiumFee();
            price += ticketPrice;
        }

        return price;
    }

    @Override
    public void addTicket() {
        System.out.println("Get ten percent off at six or more tickets");
        if(order.getTickets().size() >= 6) {
            order.setState(order.getTenPercentOffState());
        }
    }

    @Override
    public void showStudentCard() {
        System.out.println("Get second ticket for free instead of ten percent off");
        order.setState(order.getSecondTicketFreeState());
    }

    @Override
    public void chooseWeekday() {
        System.out.println("Get second ticket for free instead of ten percent off");
        order.setState(order.getSecondTicketFreeState());
    }
}
