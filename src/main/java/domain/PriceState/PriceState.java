package domain.PriceState;

import domain.PriceState.PremiumState.NonPremiumState;
import domain.PriceState.PremiumState.StudentPremiumState;
import domain.PriceState.PremiumState.PremiumState;
import domain.PriceState.PremiumState.RegularPremiumState;

public abstract class PriceState {

    private PremiumState nonPremiumState;
    private PremiumState regularPremiumState;
    private PremiumState studentPremiumState;

    private PremiumState premiumState;

    PriceState() {
        this.nonPremiumState = new NonPremiumState(this);
        this.regularPremiumState = new RegularPremiumState(this);
        this.studentPremiumState = new StudentPremiumState(this);
    }

    public PremiumState getRegularPremiumState() {
        return regularPremiumState;
    }

    public PremiumState getStudentPremiumState() {
        return studentPremiumState;
    }

    public PremiumState getPremiumState() {
        return premiumState;
    }

    public void setPremiumState(PremiumState premiumState) {
        this.premiumState = premiumState;
    }

    public abstract double calculatePrice();

    public abstract void addTicket();

    public abstract void showStudentCard();

    public abstract void chooseWeekday();
}
