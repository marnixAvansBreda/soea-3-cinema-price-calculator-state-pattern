package domain.PriceState;

import domain.MovieTicket;
import domain.Order;
import domain.PriceState.PremiumState.PremiumState;

import java.util.ArrayList;

public class TenPercentOffState extends PriceState {

    private Order order;
    private PremiumState premiumState;

    public TenPercentOffState(Order order) {
        this.order = order;
    }

    @Override
    public double calculatePrice() {
        ArrayList<MovieTicket> tickets = order.getTickets();

        double price = 0;

        for (MovieTicket ticket : tickets) {
            double ticketPrice = ticket.getPrice() + getPremiumState().getPremiumFee();
            price += ticketPrice;
        }

        return price * .9;
    }

    @Override
    public void addTicket() {
        System.out.println("Already at ten percent off");
    }

    @Override
    public void showStudentCard() {
        System.out.println("Second ticket free is better than ten percent off");
        order.setState(order.getSecondTicketFreeState());
    }

    @Override
    public void chooseWeekday() {
        System.out.println("Second ticket free is better than ten percent off");
        order.setState(order.getSecondTicketFreeState());
    }
}
