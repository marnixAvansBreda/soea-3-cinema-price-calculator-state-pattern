package domain;

import domain.PriceState.PriceState;
import domain.PriceState.RegularPriceState;
import domain.PriceState.SecondTicketFreeState;
import domain.PriceState.TenPercentOffState;

import java.util.ArrayList;

public class Order
{
    private int orderNr;
    private boolean isStudentOrder;

    private ArrayList<MovieTicket> tickets;
    private PriceState tenPercentOffState;
    private PriceState secondTicketFreeState;
    private PriceState regularPriceState;

    private PriceState priceState;

    public Order(int orderNr, boolean isStudentOrder)
    {
        this.orderNr = orderNr;
        this.isStudentOrder = isStudentOrder;

        tickets = new ArrayList<>();

        tenPercentOffState = new TenPercentOffState(this);
        secondTicketFreeState = new SecondTicketFreeState(this);
        regularPriceState = new RegularPriceState(this);

        priceState = isStudentOrder ? secondTicketFreeState : regularPriceState;
    }

    public int getOrderNr()
    {
        return orderNr;
    }

    public void addSeatReservation(MovieTicket ticket)
    {
        tickets.add(ticket);
        priceState.addTicket();
    }

    public double calculatePrice()
    {
        return priceState.calculatePrice();
    }

    public ArrayList<MovieTicket> getTickets() {
        return tickets;
    }

    public PriceState getTenPercentOffState() {
        return tenPercentOffState;
    }

    public PriceState getSecondTicketFreeState() {
        return secondTicketFreeState;
    }

    public void setState(PriceState priceState) {
        this.priceState = priceState;
    }

    public void export(TicketExportFormat exportFormat)
    {
        // Bases on the string respresentations of the tickets (toString), write
        // the ticket to a file with naming convention Order_<orderNr>.txt of
        // Order_<orderNr>.json
    }

}
