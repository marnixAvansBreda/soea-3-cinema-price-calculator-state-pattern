package domain;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

public class OrderTest {

    private Movie movie = new Movie("The Godfather");
    private LocalDateTime saturday = LocalDateTime.of(LocalDate.of(2019, 1, 26), LocalTime.of(0, 0));
    private LocalDateTime thursday = LocalDateTime.of(LocalDate.of(2019, 1, 31), LocalTime.of(0, 0));
    private MovieScreening msWeekday = new MovieScreening(movie, thursday, 9.0);
    private MovieScreening msWeekend = new MovieScreening(movie, saturday, 13);

    @Test
    public void noTickets() {
        Order order = new Order(1, false);

        double price = order.calculatePrice();

        assertEquals(0.0, price, 0);
    }

    @Test
    public void secondTicketWeekend() {
        Order order = new Order(1, false);

        order.addSeatReservation(new MovieTicket(msWeekend, false, 1, 1));
        order.addSeatReservation(new MovieTicket(msWeekend, false, 1, 2));

        double price = order.calculatePrice();

        assertEquals(26, price, 0);
    }

    @Test
    public void secondTicketStudentWeekend() {
        Order order = new Order(1, true);

        order.addSeatReservation(new MovieTicket(msWeekend, false, 1, 1));
        order.addSeatReservation(new MovieTicket(msWeekend, false, 1, 2));
        order.addSeatReservation(new MovieTicket(msWeekend, false, 1, 3));
        order.addSeatReservation(new MovieTicket(msWeekend, false, 1, 4));

        double price = order.calculatePrice();

        assertEquals(26, price, 0);
    }

    @Test
    public void secondTicketWeekday() {
        Order order = new Order(1, false);

        order.addSeatReservation(new MovieTicket(msWeekday, false, 1, 1));
        order.addSeatReservation(new MovieTicket(msWeekday, false, 1, 2));
        order.addSeatReservation(new MovieTicket(msWeekday, false, 1, 3));

        double price = order.calculatePrice();

        assertEquals(18, price, 0);
    }

    @Test
    public void premium() {
        Order order = new Order(1, false);

        order.addSeatReservation(new MovieTicket(msWeekday, true, 1, 1));

        double price = order.calculatePrice();

        assertEquals(12, price, 0);
    }

    @Test
    public void studentLessThanSixTickets() {
        Order order = new Order(1, true);

        order.addSeatReservation(new MovieTicket(msWeekday, true, 1, 1));
        order.addSeatReservation(new MovieTicket(msWeekday, true, 1, 2));

        double price = order.calculatePrice();

        assertEquals(11, price, 0);
    }

    @Test
    public void studentSixOrMoreTickets() {
        Order order = new Order(1, true);

        order.addSeatReservation(new MovieTicket(msWeekday, false, 1, 1));
        order.addSeatReservation(new MovieTicket(msWeekday, false, 1, 2));
        order.addSeatReservation(new MovieTicket(msWeekday, false, 1, 3));
        order.addSeatReservation(new MovieTicket(msWeekday, false, 1, 4));
        order.addSeatReservation(new MovieTicket(msWeekday, false, 1, 5));
        order.addSeatReservation(new MovieTicket(msWeekday, false, 1, 6));

        double price = order.calculatePrice();

        assertEquals(27, price, 0);
    }

    @Test
    public void sixOrMoreTickets() {
        Order order = new Order(1, false);

        order.addSeatReservation(new MovieTicket(msWeekend, true, 1, 1));
        order.addSeatReservation(new MovieTicket(msWeekend, true, 1, 2));
        order.addSeatReservation(new MovieTicket(msWeekend, true, 1, 3));
        order.addSeatReservation(new MovieTicket(msWeekend, true, 1, 4));
        order.addSeatReservation(new MovieTicket(msWeekend, true, 1, 5));
        order.addSeatReservation(new MovieTicket(msWeekend, true, 1, 6));
        order.addSeatReservation(new MovieTicket(msWeekend, true, 1, 7));

        double price = order.calculatePrice();

        assertEquals(100.8, price, 0);
    }

    @Test
    public void getOrderNr() {
        Order order = new Order(1, false);

        assertEquals(1, order.getOrderNr());
    }
}
